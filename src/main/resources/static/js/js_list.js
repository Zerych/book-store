/**
 * Created by zeruch on 26/04/17.
 */
$(document).ready(function () {
    // get all books from server
    var request = new XMLHttpRequest();
    request.open('GET', '/books', false);
    request.send();

    var books = $.parseJSON(request.responseText).sort();
    console.log(books);


    var table = document.getElementById("tableGrid");
    var listBought = document.getElementById("boughtList");
    var spanPrice = document.getElementById("price");
    var spanEarned = document.getElementById("moneyEarned");
    var totalPrice = 0;
    var moneyEarned = 0;
    var boughtBooks = [];

    // go through all books and add them to the table
    for (var i = 0; i < books.length; i++) {
        moneyEarned += books[i].price * books[i].timesBought;

        var newRow = document.createElement('tr');
        newRow.innerHTML = "<td>"
            + books[i].id + "</td><td>"
            + books[i].name + "</td><td>"
            + books[i].price + " $</td><td>"
            + books[i].timesBought + "</td>";
        newRow.id = books[i].id;

        table.appendChild(newRow);
        // add listener for each row to add the book to cart
        document.getElementById(newRow.id).addEventListener("click", function (e) {

            // add book to list
            boughtBooks.push(books[e.target.parentNode.id - 1]);
            console.log(boughtBooks);

            // update total price
            totalPrice += books[e.target.parentNode.id - 1].price;
            spanPrice.innerHTML = totalPrice.toFixed(2);

            // add book to cart list
            var newListItem = document.createElement('li');
            newListItem.innerHTML = books[e.target.parentNode.id - 1].name;
            listBought.appendChild(newListItem);
        });
    }
    spanEarned.innerHTML = moneyEarned.toFixed(2);

    // listener for checkout button
    document.getElementById("checkout").addEventListener("click", function () {

        if (boughtBooks.length != 0) {
            request.open('PUT', '/books', false);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify(boughtBooks));

            location.reload();
        }
        else
            alert("You haven't selected any books to buy!");
    });

    // listener for cancel order button
    document.getElementById("cancelOrder").addEventListener("click", function () {
        boughtBooks = [];
        totalPrice = 0;
        listBought.innerHTML = "";
        spanPrice.innerHTML = 0;
    });

    // listener for addBook button
    try {
        document.getElementById("addBook").addEventListener("click", function () {
            var bookName = document.getElementById("addBookName").value;
            var bookPrice = document.getElementById("addBookPrice").value;

            if (bookName.length > 0 && bookPrice.length > 0) {
                request.open('POST', '/books', false);
                request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                request.send(JSON.stringify({name: bookName, price: bookPrice}));

                location.reload();
            }
            else
                alert("You've entered invalid data!");
        });
        document.getElementById("showAddBook").addEventListener("click", function () {
            this.style.display = "none";
            document.getElementById("addBookBlock").style.display = "block";
        });
    }
    catch (e) {
        console.log(e);
    }


});