/**
 * Created by zeruch on 26/04/17.
 */
package com.funtime.requests;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CreateBookRequest {
    private String name;
    private BigDecimal price;
}
