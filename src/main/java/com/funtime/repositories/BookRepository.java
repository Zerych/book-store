/**
 * Created by zeruch on 26/04/17.
 */
package com.funtime.repositories;

import com.funtime.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    @Modifying
    @Query("UPDATE Book b set b.timesBought = b.timesBought + 1 WHERE b.id = :id")
    void incrementTimesBought(@Param("id") Long id);

    List<Book> findAllByOrderByIdAsc();
}
