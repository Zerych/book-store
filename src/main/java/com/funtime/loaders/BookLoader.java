/**
 * Created by zeruch on 26/04/17.
 */
package com.funtime.loaders;

import com.funtime.domain.Book;
import com.funtime.repositories.BookRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class BookLoader implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOG = Logger.getLogger(BookLoader.class);

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        Book book1 = new Book();
        book1.setName("Thinking in Java");
        book1.setPrice(new BigDecimal("99.99"));
        bookRepository.save(book1);
        LOG.info("Saved book: " + book1);

        Book book2 = new Book();
        book2.setName("Harry Potter");
        book2.setPrice(new BigDecimal("85.90"));
        bookRepository.save(book2);
        LOG.info("Saved book: " + book2);

        Book book3 = new Book();
        book3.setName("Tales");
        book3.setPrice(new BigDecimal("35.01"));
        bookRepository.save(book3);
        LOG.info("Saved book: " + book3);

        Book book4 = new Book();
        book4.setName("Book of Souls");
        book4.setPrice(new BigDecimal("666.66"));
        bookRepository.save(book4);
        LOG.info("Saved book: " + book4);
    }
}
