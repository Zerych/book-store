/**
 * Created by zeruch on 26/04/17.
 */
package com.funtime.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BookDto {

    private Long id;
    private String name;
    private BigDecimal price;
    private Long timesBought;

    public BookDto() {
    }

    public BookDto(Book book) {
        this(new BookDtoBuilder(book.getId()).
                name(book.getName()).
                price(book.getPrice()).
                timesBought(book.getTimesBought()));
    }

    private BookDto(BookDtoBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.price = builder.price;
        this.timesBought = builder.timesBought;
    }

    public static class BookDtoBuilder {
        private Long id;
        private String name;
        private BigDecimal price;
        private Long timesBought;

        public BookDtoBuilder(Long id) {
            this.id = id;
        }

        public BookDtoBuilder name(String name) {
            this.name = name;
            return this;
        }

        public BookDtoBuilder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public BookDtoBuilder timesBought(Long timesBought) {
            this.timesBought = timesBought;
            return this;
        }

        public BookDto build() {
            return new BookDto(this);
        }
    }
}
