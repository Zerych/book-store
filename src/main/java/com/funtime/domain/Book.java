/**
 * Created by zeruch on 26/04/17.
 */
package com.funtime.domain;

import com.funtime.requests.CreateBookRequest;
import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(generator = "books_id_gen")
    @GenericGenerator(
            name = "books_id_gen",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "books_id_seq"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    private String name;

    private BigDecimal price;

    private Long timesBought = 0L;

    public Book() {

    }

    public Book(CreateBookRequest request) {
        this.name = request.getName();
        this.price = request.getPrice();
    }
}
