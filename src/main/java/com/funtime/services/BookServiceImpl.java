/**
 * Created by zeruch on 26/04/17.
 */
package com.funtime.services;

import com.funtime.domain.Book;
import com.funtime.domain.BookDto;
import com.funtime.repositories.BookRepository;
import com.funtime.requests.CreateBookRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookDto> findAll() {
        List<Book> books = bookRepository.findAllByOrderByIdAsc();
        List<BookDto> bookDtos = new ArrayList<>();
        for (Book book : books) {
            BookDto bookDto = new BookDto(book);
            bookDtos.add(bookDto);
        }

        return bookDtos;
    }

    @Override
    @Transactional
    public void buyBooks(List<BookDto> books) {
        for (BookDto book : books) {
            bookRepository.incrementTimesBought(book.getId());
        }
    }

    @Override
    public void createBook(CreateBookRequest request) {
        bookRepository.save(new Book(request));
    }
}
