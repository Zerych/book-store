/**
 * Created by zeruch on 26/04/17.
 */
package com.funtime.services;

import com.funtime.domain.BookDto;
import com.funtime.requests.CreateBookRequest;

import java.util.List;

public interface BookService {

    List<BookDto> findAll();

    void buyBooks(List<BookDto> books);

    void createBook(CreateBookRequest request);
}
