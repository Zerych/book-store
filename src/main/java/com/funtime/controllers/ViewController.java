/**
 * Created by zeruch on 25/04/17.
 */
package com.funtime.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @RequestMapping({"/", "index"})
    public String index() {
        return "index";
    }
}
