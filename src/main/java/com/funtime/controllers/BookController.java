/**
 * Created by zeruch on 26/04/17.
 */
package com.funtime.controllers;

import com.funtime.domain.BookDto;
import com.funtime.requests.CreateBookRequest;
import com.funtime.services.BookService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    private static final Logger LOG = Logger.getLogger(BookController.class);

    private BookService bookService;

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<BookDto>> getAllBooks() {
        LOG.info("getAllBooks()");
        List<BookDto> bookDtos = bookService.findAll();

        return ResponseEntity.ok(bookDtos);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity buyBooks(@RequestBody List<BookDto> books) {
        LOG.info("buyBooks(" + books + ")");
        bookService.buyBooks(books);

        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createBook(@RequestBody CreateBookRequest request) {
        LOG.info("createBook(" + request + ")");
        bookService.createBook(request);

        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }
}
