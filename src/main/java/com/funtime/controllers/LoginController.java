/**
 * Created by zeruch on 25/04/17.
 */
package com.funtime.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class LoginController {

    @RequestMapping("login")
    public String login(Model model,
                        @RequestParam Map<String, String> requestParams) {

        String param1 = requestParams.get("error");
        String param2 = requestParams.get("logout");

        if (param1 != null) model.addAttribute("loginFailure", param1);
        if (param2 != null) model.addAttribute("logoutSuccess", param2);

        return "login";
    }
}
