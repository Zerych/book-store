Book Store test project.

===============================

To launch the project:

1. Edit your profile with DB credentials at *src/main/resources/application.yml*
2. Build the project with Maven: *mvn clean package*
3. Launch it: *java -jar target/book-store-0.0.1-SNAPSHOT.jar*